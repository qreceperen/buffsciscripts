﻿#Clears the console
clear-host

#Generates a random number between 1 and 100, and stores it in a variable called $magicNumber
$magicNumber = Get-Random  -Minimum 1 -Maximum 1000
#Initializes a variable called $tries (to zero) that will keep track of how many guesses the user attempts.
$tries = 0
#Resets the $guess variable (to zero) for each new run of the script.
$guess=0

#As long as the user's guess is not equal to the random number generated...
while ($guess -ne $magicNumber)
{
#Read the user's numeric guess and store it in a variable called $guess. Read-Host takes console input and stores it as a string, so we need to cast that string into an int, so it can
#be treated like a number and not a string.
[int] $guess = Read-Host "Guess my magic number (1-100)"
#Since the user has just guessed a number, increment the variable $tries by 1.
$tries++  #Same thing as $tries=tries+1

#If the user's guess equals the randomly generated number...
if ($guess -eq $magicNumber)
{
#Display a congratulatory message. The other conditions are not evaluated, and the program loops back to Line 9, when the check condition is now false. Then the program goes to the very end.
Write-Host "Winner Winner Chicken Dinner!"
}

#If the user's guess didn't match the randomly generated number, check to see that the guess is less than the randomly generated number.
elseif ($guess -lt $magicNumber)
{
#If it is, tell the user the next guess needs to be higher, and the program loops back to the check condition on Line 9. The condition is still true, so we enter the loop again.
Write-Host "Higher!"
}

#If the previous two conditions were false, we don't need to specify this condition: elseif ($guess -gt $magicNumber), since there's only one possibility left,
else
{
#Tell the user the next guess needs to be lower, since the guess was higher than the randomly generated number
#The program loops back to the check condition on Line 9. The condition is still true, so we enter the loop again.
Write-Host "Lower"
}

} #while loop ends here


#Now that the loop has been exited, the user has sucessfully guessed the number. 

#If the user's number of attempted guesses was less than or equal to 5...
if ($tries -le 5)
{
#Tell them that they did a great job.
Write-Host "Great! Just $tries tries!"
}

#Or maybe the number of guesses was less than or equal to 10.
elseif ($tries -le 10)
{
#Tell them that they did an ok job.
Write-Host "Eh! $tries tries is ok..."
}

#If the number of guesses was more than 10...
else
{
#Tell them that you're not impressed.
Write-Host "You didn't impress me at all! It took you $tries tries!"
}

#Clear-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/functions/clear-host?view=powershell-5.1
#Read-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/read-host?view=powershell-5.1
#Write Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-host?view=powershell-5.1
#Comparison Operators: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators?view=powershell-5.1