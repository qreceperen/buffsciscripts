#Clears the console
Clear-Host

Write-Output "
1. See a list of processes on the system
2. See a list of services on the system
3. Send a ping
4. Double a number

"

$option = Read-Host "Please choose one of the options above with numbers 1,2,3,or 4"

if ($option -eq 1)
{
    Write-Output "You chose 1"
    Write-Output "Here are the processes on the system:"

    Get-Process
}
elseif ($option -eq 2)
{
    Write-Output "You chose 2"
    Write-Output "Here are the services on the system:"

    Get-Service
}

elseif ($option -eq 3)
{
    Write-Output "You chose 3"
    $pingee = Read-Host "Please enter IP address or FQDN"
    Test-Connection -ComputerName $pingee 
}

elseif ($option -eq 4)
{
    Write-Output "You chose 4"

    $num1 = Read-Host "Please enter a number"
    $num2 = [int]$num1*2
    Write-Output "Your original number was $num1, but now it’s $num2."
}

else {
    Write-Output "Please choose a valid option between 1 and 4!"
    

}
