﻿#Clears the console
Clear-Host

#Declares a function called Jonathan-Ping
function Jonathan-Ping

{
#Defines a parameter called $pingees, an array of string inputs that can be passed to this function.
#If no parameters are specified when this function is called, localhost will be used by default.
  param([string[]]$pingees = "localhost")

#Loops through all elements of the $pingees array, storing each input string into $pingee for each loop iteration.
#For example, if the user ran the function like this: Jonathan-Ping 8.8.8.8, 8.8.4.4, 9.9.9.9, www.google.com, www.rit.edu 
#the loop would iterate 5 times. For the first iteration, 8.8.8.8 will be stored in $pingee. For the second iteration, 8.8.4.4 will be stored in $pingee.
#For the third iteration, 9.9.9.9 will be stored in $pingee. For the fourth iteration, www.google.com will be stored in $pingee. 
#For the fifth iteration, www.rit.edu will be stored in pingee.
  foreach ($pingee in $pingees)
  {

  #Test-Connection sends ICMP Echo Requests, just like ping. The current value of $pingee (explained above) will be sent 2 ICMP Echo Requests (-Count 2), and no output will be shown (-Quiet).
  #If ICMP Echo Replies come back in response...
     if(Test-Connection -ComputerName $pingee -Count 2 -Quiet)
       {
       #Print a message of success to the console.
       Write-Output "$pingee is up."
       }
     #Otherwise,
      else
      {
      #Print a message of failure to the console.
      Write-Warning "$pingee is down (or isn't responding to ICMP)."
      }

  }
}

Jonathan-Ping 8.8.8.8, 8.8.4.4, 9.9.9.9, www.google.com, www.rit.edu



#Clear-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/functions/clear-host?view=powershell-5.1
#Test-Connection: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/test-connection?view=powershell-5.1
#Write-Output: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-output?view=powershell-5.1
#Write-Warning: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-warning?view=powershell-5.1
