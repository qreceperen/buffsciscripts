﻿#Clears the console
Clear-Host

#Prompts the user to enter a number, which is stored in a variable called $num1.
$num1 = Read-Host "Please enter a number"
#Prompts the user to enter another number, which is stored in a variable called $num2.
$num2 = Read-Host "Please enter another number"

#Checks to see if the first number is greater than the second number
if ($num1 -gt $num2)
{
#If the first number is greater than the second number, print out a message indicating it. After Line 10 executes, the other checks are skipped, and execution continues at the end (Line 30).
Write-Host $num1 is greater than $num2
}


#If the first number is not greater than the second number, checks to see if the second number is greater than the first number.
elseif ($num2 -gt $num1)
{
#If the second number is greater than the first number, print out a message indicating it. After Line 16 executes, the program continues at Line 30, skipping the else block.
Write-Host $num2 is greater than $num1
}


#If neither the condition on Line 7 nor the condition on Line 15 was true, this block will kick in and execute.
else
{
#Print a message indicating the only possible conclusion.
Write-Host "They're the same!!!"
}

#The program will always conclude with this line
Write-Host "Thanks for playing :-)"

#Clear-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/functions/clear-host?view=powershell-5.1
#Read-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/read-host?view=powershell-5.1
#Write Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-host?view=powershell-5.1
#Comparison Operators: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators?view=powershell-5.1