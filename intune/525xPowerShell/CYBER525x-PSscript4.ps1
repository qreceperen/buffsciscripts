﻿#Removes all text from the current display
Clear-Host
#Resets a variable, explained below.
$Match=0 

#Prompts the user to enter a machine name or IP address, and stores the user's answer as a string in a variable called $Computer
$Computer = Read-Host "Which computer do you want to monitor?" 

#Gets a list of services on the specified machine, and stores the results in an array called $GetService
$GetService = Get-Service -ComputerName $Computer

#Prompts the user to enter the name of a service on the given machine to check, and stores the user's answer as a string in a variable called $srv.
$Srv = Read-Host "Enter a Windows service name"

#Loops through each element in the $GetService array (representing each service on the machine), storing each service object in a variable called $service with each
#loop iteration. The first time the loop runs, $service stores the first service found in $GetService. The second time the loop runs, $service stores the second service
#found in $GetService, etc. 
foreach ($Service in $GetService)
{
        #If the name of the service on the current loop iteration matches the service name that the user specified, above, let's check if the service is stopped or running.
        if ($Service.name -eq $Srv)
            {
            #Gives a value of 1 to a variable called $Match, to determine if a service was never found (see end of script)
            $Match=1
                #If that service is stopped...    
                if ($Service.status -eq "Stopped")
                {
                    #Write a message to the console, using a specified color scheme.
                    Write-Host "Service is stopped. Starting..." -ForegroundColor White -BackgroundColor Red
                    #Start the service. Get-Service with the specified computer name and service name generates an object, which is passed to Start-Service.
                    Start-Service -InputObject (Get-Service -ComputerName $Computer -Name $Srv)
                    #Gets an object representing the service that was stopped, and just now started, and shows the new status.
                    Get-Service -InputObject $Service
                    }
                #If that service is running...
                elseif ($Service.Status -eq "Running")
                {
                    #Write a message to the console, using a specified color scheme.
                    Write-Host "The service is running." -ForegroundColor Blue -BackgroundColor Cyan
                }
                #If that service is not running or stopped (For example, StopPending indicates a hung service)
                else
                {
                    #Write a message to the console, using a specified color scheme.
                    Write-Host "The service is running." -ForegroundColor Black -BackgroundColor Yellow
                }

            }
         
          }
      #If the variable $Match is not equal to 1, which means the script never hit line 22, because the user entered service name was not found in the machine's service list....
      if ($Match -ne 1)
      {
       #Write a message to the console, using a specified color scheme.
       Write-Host "Never found your service!" -ForegroundColor Green -BackgroundColor Black
      }

#Clear-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/functions/clear-host?view=powershell-5.1
#Read-Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/read-host?view=powershell-5.1
#Get-Service: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-service?view=powershell-5.1
#Write Host: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-host?view=powershell-5.1
#Start-Service: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/start-service?view=powershell-5.1
#Comparison Operators: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators?view=powershell-5.1