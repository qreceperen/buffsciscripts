BackupToAAD-BitLockerKeyProtector `
-MountPoint $env:SystemDrive `
-KeyProtectorId ((Get-BitLockerVolume -MountPoint $env:SystemDrive ).KeyProtector | `
Where-Object {$_.KeyProtectorType -eq "RecoveryPassword" }).KeyProtectorId

Enable-BitLocker -MountPoint "C:" `
-EncryptionMethod Aes256 -UsedSpaceOnly 

