$log_file_name_start = "remove_KB11";

$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;

if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
} else {
    $runVer = 1;
}

$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"

$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"

Start-Transcript -Path $transcriptPath -Force # -NoClobber

Write-Output "Start Heyo1"

# wmic qfe list brief
# Get-WmiObject Win32_QuickFixEngineering
# wmic qfe list brief | findstr "808 | 802"
# wusa /uninstall /KB:5000808 /quiet /warnrestart

# dism /online /get-packages /format:table

$packageNames = 'Package_for_RollupFix~31bf3856ad364e35~amd64~~19041.867.1.8',
'Package_for_RollupFix~31bf3856ad364e35~amd64~~18362.1440.1.7' -join ' | '

$updatePackage = (DISM /Online /Get-Packages | Select-String $packageNames).matches.value

if ($updatePackage) {
    Write-Host "Will remove package $updatePackage"
    # DISM.exe /Online /Remove-Package /PackageName:$updatePackage /quiet /norestart /LogPath:c:\temp\updateremoval.log /LogLevel:3
}else{
    Write-Host "No Packages to remove"
}

$packagenames = 'Package_for_RollupFix~31bf3856ad364e35~amd64~~19041.867.1.8',
                'Package_for_RollupFix~31bf3856ad364e35~amd64~~18362.1440.1.7' -join '|'
                
$update = (DISM /Online /Get-Packages | Select-String $packagenames).matches.value



# Get-WindowsPackage -Online | ?{$_.ReleaseType -like "*Update*"} | %{Get-WindowsPackage -Online -PackageName $_.PackageName} | ?{$_.Description -like "*KB5000802*"} | Remove-WindowsPackage -Online -NoRestart
# Get-WindowsPackage -Online | Where-Object{$_.ReleaseType -like "*Update*"} | ForEach-Object{Get-WindowsPackage -Online -PackageName $_.PackageName} | Where-Object{$_.Description -like "*KB5000802*"} | Remove-WindowsPackage -Online -NoRestart

Write-Output "Before Showing Notifications"    

# Notification won't show when run in system context
<#
# if ($updatePackage) {
if (Test-Path "c:\temp\updateremoval.log") {
    $SEL = Select-String -Path "c:\temp\updateremoval.log" -Pattern "Reboot required=yes"
    if ($SEL) {
    Write-Output "Contains String Reboot required=yes"
    
    Import-Module AnyBox
    Show-AnyBox -Title 'BuffSci IT: Please Restart ASAP!' -Message 'Latest Windows update problem is fixed now. Please Restart your computer as soon as possible today.' -FontSize 16 -ContentAlignment 'Center' -Buttons 'OK' -Topmost `
        -Icon 'Warning' -BackgroundColor 'White' -FontColor 'Blue' -FontFamily 'Comic Sans MS'


    Import-Module BurntToast
    New-BurntToastNotification -Text 'BuffSci IT: Please Restart ASAP!', 'Latest Windows update problem is fixed now. Please Restart your computer as soon as possible today.' -Sound 'Alarm2' -SnoozeAndDismiss


    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK #YesNoCancel
    $MessageIcon = [System.Windows.MessageBoxImage]::Warning
    $MessageBody = "Latest Windows update problem(causing Blue Screen of Death while trying to print) is fixed now. Please Restart your computer as soon as possible today."
    $MessageTitle = "Please Restart!"

    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)

    Write-Host "Your choice is $Result"


    } else {
        Write-Output "Does not Contain String Reboot required=yes"
    }
}
#>

Stop-Service wuauserv
$pause = (Get-Date).AddDays(15)
$pause = $pause.ToUniversalTime().ToString( "yyyy-MM-ddTHH:mm:ssZ" )
$pause_start = (Get-Date)
$pause_start = $pause_start.ToUniversalTime().ToString( "yyyy-MM-ddTHH:mm:ssZ" )
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseUpdatesExpiryTime' -Value $pause                                                 
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseFeatureUpdatesStartTime' -Value $pause_start
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseFeatureUpdatesEndTime' -Value $pause
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseQualityUpdatesStartTime' -Value $pause_start
Set-itemproperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseQualityUpdatesEndTime' -Value $pause
Stop-Service wuauserv


# Set-ItemProperty -Path 'HKLM:SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' -Name 'PauseUpdatesExpiryTime' -Value "2021-03-24T17:19:27Z"

Write-Output "After Showing Notifications"    

Get-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings' | Select-Object PauseUpdatesExpiryTime



# net stop wuauserv
# net stop bits
# net stop dosvc

# net start wuauserc
# net start bits
# net start dosvc

Write-Output "End Heyo1"
Stop-Transcript
