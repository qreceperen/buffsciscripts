Install-Module MSOnline
Connect-MsolService

Once connected you can run the below cmdlet to disable 2-step-verification for a single user

Set-MsolUser -UserPrincipalName user@bascs.org -StrongAuthenticationRequirements @()

For multiple users you can use the below cmdlet: ()
Get-MsolUser -MaxResults 2000 | Set-MsolUser -StrongAuthenticationRequirements @()