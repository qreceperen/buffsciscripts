Add-Type -AssemblyName System.speech;
$speak = New-Object System.Speech.Synthesis.SpeechSynthesizer;
$speak.Rate = -2;
$speak.Speak('Hi Ethem, How Are you! The current date and time is '+$(Get-Date));


$secpasswd = ConvertTo-SecureString "dvemmzbkjnpmxczw" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("helpdesk@bascs.org", $secpasswd)
Send-MailMessage -From 'User01 <helpdesk@bascs.org>' -To 'IT <sen@bascs.org>', 'User03 <ahmetshen.us@gmail.com>' -Subject 'Sending the Attachment' -Body "Forgot to send the attachment. Sending now." -Attachments '.\Rename.1.log' -Priority High -DeliveryNotificationOption OnSuccess, OnFailure -SmtpServer 'smtp.gmail.com' -UseSsl -Port 587 -Credential $cred 

$gmailCred = Get-Credential

$sendMailParams = @{
    From = 'adbertram@gmail.com' ## Must be gmail.com
    To = 'someemail@domain.com'
    Subject = 'some subject'
    Body = 'some body'
    SMTPServer = 'smtp.gmail.com'
    SMTPPort = 587
    UseSsl = $true
    Credential = $gmailCred
}

Send-MailMessage @sendMailParams


Function Get-Something {
    <#
    .SYNOPSIS
        This is a basic overview of what the script is used for..
     
     
    .NOTES
        Name: Get-Something
        Author: theSysadminChannel
        Version: 1.0
        DateCreated: 2020-Dec-10
     
     
    .EXAMPLE
        Get-Something -UserPrincipalName "username@thesysadminchannel.com"
     
     
    .LINK
        https://thesysadminchannel.com/powershell-template -
    #>
     
        [CmdletBinding()]
        param(
            [Parameter(
                Mandatory = $false,
                ValueFromPipeline = $true,
                ValueFromPipelineByPropertyName = $true,
                Position = 0
                )]
            [string[]]  $UserPrincipalName
        )
     
        BEGIN {}
     
        PROCESS {}
     
        END {}
    }


$Path = $env:TEMP
$Installer = "FileName.exe"
Invoke-WebRequest -Uri "https://workingwebsitelink" -OutFile $Path\$Installer
Start-Process -FilePath $Path\$Installer -Wait
Remove-Item $Path\$Installer



$A = New-ScheduledTaskAction -Execute "Taskmgr.exe"
$T = New-ScheduledTaskTrigger -AtLogon
$P = New-ScheduledTaskPrincipal "Contoso\Administrator"
$S = New-ScheduledTaskSettingsSet
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S
Register-ScheduledTask T1 -InputObject $D


$O = New-ScheduledJobOption -RunElevated
# $T = New-JobTrigger -Weekly -At "9:00 PM" -DaysOfWeek Monday -WeeksInterval 2
$T = New-JobTrigger -AtLogon
$path = "test-connection.ps1"
Register-ScheduledJob -Name "run Taskmgr at logon" -FilePath $path -ScheduledJobOption $O -Trigger $T


$powershell_code_to_run = {
    # ALL CODE TO RUN GOES HERE
    $source = "PowerShellScript"
    $log_sources = (Get-EventLog -LogName "Application").Source | Select-Object -Unique
    if($log_sources -notcontains $source) {
        New-EventLog -LogName Application -Source $source
    }
    Write-EventLog -LogName Application -Source $source -EntryType Error -EventId 1337 -Message "This is a message."
    # AND END HERE.
}

# Base64 encode code to run.
$encodedCommand = [System.Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($powershell_code_to_run.ToString()))

# Action, using EncodedCommand removes the need to escape quotes etc.
$action = New-ScheduledTaskAction -Execute 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' -Argument "-WindowStyle Hidden -EncodedCommand $encodedCommand"

# Task trigger                                  Start :00 next hour                             Repeat every 30 minutes, repeat forever
$triggers = New-ScheduledTaskTrigger -Once -At ((Get-Date).AddMinutes(60 - (Get-Date).Minute)) -RepetitionInterval (New-TimeSpan -Minutes 30)
#                                                Start 1 minute from now          Repeat every 30 minutes, repeat forever
# $triggers = New-ScheduledTaskTrigger -Once -At ([datetime]::Now.AddMinutes(1)) -RepetitionInterval (New-TimeSpan -Minutes 30)

# Settings for the task.
$settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -StartWhenAvailable

# Other settings
$principal = New-ScheduledTaskPrincipal -RunLevel Highest -UserId "LOCALSERVICE" -LogonType ServiceAccount

# Name of the task
$taskName = 'Some task name'

# The actual task object
$task = New-ScheduledTask -Action $action -Settings $settings -Trigger $triggers -Description 'Check a folder if there has been any changes in the last 30 minutes.' -Principal $principal

# Remove the old task
Unregister-ScheduledTask -TaskName $taskName -Confirm:$false -ErrorAction SilentlyContinue

#  Create new
Register-ScheduledTask -TaskName $taskName -InputObject $task -TaskPath '\'





