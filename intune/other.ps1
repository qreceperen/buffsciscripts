Change MFA state using PowerShell
Install-Module MSOnline -Scope CurrentUser
Connect-MsolService

https://docs.microsoft.com/en-us/azure/active-directory/authentication/howto-mfa-userstates

Add-Type -AssemblyName PresentationFramework
[System.Windows.MessageBox]::Show('Please Restart ASAP!')



$SyncedLibraries = foreach ($inifile in $IniFiles) {
    $IniContent = get-content $inifile.fullname -Encoding Unicode
    [PSCustomObject]@{
        'Item Count' = ($IniContent | Where-Object { $_ -like 'ItemCount*' }) -split '= ' | Select-Object -last 1
        'Site Name'  = ($IniContent | Where-Object { $_ -like 'SiteTitle*' }) -split '= ' | Select-Object -last 1
        'Site URL'   = ($IniContent | Where-Object { $_ -like 'DavUrlNamespace*' }) -split '= ' | Select-Object -last 1
    }
}

if (($SyncedLibraries.'Item count' | Measure-Object -Sum).sum -gt '280000') { 
    write-host "Unhealthy - Currently syncing more than 280k files. Please investigate."
    $SyncedLibraries
    }


$action = New-ScheduledTaskAction -Execute “notepad.exe” -Argument "FileNameLocation"
$trigger = New-ScheduledTaskTrigger -AtLogOn
$principal = New-ScheduledTaskPrincipal -UserId (Get-CimInstance –ClassName Win32_ComputerSystem | Select-Object -expand UserName)
$task = New-ScheduledTask -Action $action -Trigger $trigger -Principal $principal
Register-ScheduledTask Notepad -InputObject $task
Start-ScheduledTask -TaskName Notepad
Start-Sleep -Seconds 5
Unregister-ScheduledTask -TaskName notepad -Confirm:$false


Add-Type -AssemblyName System.Windows.Forms
$global:balmsg = New-Object System.Windows.Forms.NotifyIcon
$path = (Get-Process -id $pid).Path
$balmsg.Icon = [System.Drawing.Icon]::ExtractAssociatedIcon($path)
$balmsg.BalloonTipIcon = [System.Windows.Forms.ToolTipIcon]::Warning
$balmsg.BalloonTipText = ‘This is the pop-up message text for the Windows 10 user'
$balmsg.BalloonTipTitle = "Attention $Env:USERNAME"
$balmsg.Visible = $true
$balmsg.ShowBalloonTip(20000)




If (!(SYSTEMINFO.exe | findstr KB5000802)) {
    If (!(SYSTEMINFO.exe | findstr KB5000808)) {
        Write-Host "Hi"
    # exit
    }
}
   

# The script is provided "AS IS" with no warranties.
$url = "http://www.7-zip.org/a/7z1701-x64.msi"
$filePath = "c:\windows\temp\7z1701-x64.msi"
$ProgressPreference = 0
Invoke-WebRequest $url -OutFile $filePath -UseBasicParsing
& $filePath /quiet


If(-not(Get-InstalledModule SQLServer -ErrorAction silentlycontinue)){
    Install-Module SQLServer -Confirm:$False -Force
}



# The script is provided "AS IS" with no warranties.
 
Param([switch]$Is64Bit = $false)
 
Function Restart-As64BitProcess
{
If ([System.Environment]::Is64BitProcess) { return }
$Invocation = $($MyInvocation.PSCommandPath)
if ($Invocation -eq $null) { return }
$sysNativePath = $psHome.ToLower().Replace("syswow64", "sysnative")
Start-Process "$sysNativePath\powershell.exe" -ArgumentList "-ex bypass -file `"$Invocation`" -Is64Bit" -WindowStyle Hidden -Wait
}
 
Function New-RegistryKey
{
Param([Parameter(Mandatory = $true)]
[string]$Key,
[Parameter(Mandatory = $true)]
[string]$Name,
[Parameter(Mandatory = $true)]
[string]$Value,
[ValidateSet("String", "ExpandString", "Binary", "DWord", "Multistring", "QWord", ignorecase=$true)]
[string]$Type = "String")
try
{
$subkeys = $Key.split("\")
 
foreach ($subkey in $subkeys)
{
$currentkey += ($subkey + '\')
if (!(Test-Path $currentkey))
{
New-Item -Type String -Path $currentkey | Out-Null
}
}
 
Set-ItemProperty -Path $currentkey -Name $Name -Value $Value -Type $Type -ErrorAction SilentlyContinue
}
catch [system.exception]
{
$message = "{0} threw an exception: `n{0}" -f $MyInvocation.MyCommand, $_.Exception.ToString()
Write-Host $message
}
}
 
if (!$Is64Bit) { Restart-As64BitProcess }
else
{
# Enable Potentially Unwanted Application protection
New-RegistryKey -Key "hklm:\SOFTWARE\Microsoft\Windows Defender" -Name "PUAProtection" -Value "1" -Type DWord
}


TimeZone

Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\tzautoupdate -Name Start -Value "3"
Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\tzautoupdate -Name Start -Value "4"

