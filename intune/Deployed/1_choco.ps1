# Set-ExecutionPolicy Bypass -Scope Process -Force; 
# iex ((New-Object System.Net.WebClient).DownloadString('http://internal_raw_url/ChocolateyInstall.ps1'))


# $testchoco = powershell choco -v
# if(-not($testchoco)){

$log_file_name_start = "1_choco.ps1_2";
$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}
$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"

Add-Type -AssemblyName System.speech
$speak = New-Object System.Speech.Synthesis.SpeechSynthesizer


if (-not(test-path "C:\ProgramData\chocolatey\choco.exe")) { 
    Write-Output "Seems like Chocolatey is not installed, installing now, then will Restart"
    $speak.Speak("Seems like Chocolatey is not installed, installing now, then will Restart")

    Set-ExecutionPolicy Bypass -Scope Process -Force;
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; 
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'));
    
    choco feature enable -n=allowGlobalConfirmation
    Write-Output "End Heyo1"
    Stop-Transcript

    $speak.Speak('Chocolatey is not installed, will force Restart now.')
    $speak.Speak('If the computer does not restart, please force restart yourself manually')
    Restart-Computer -Force 
    # Restart-Computer restarts the remote computer and then waits up to 5 minutes (300 seconds) for PowerShell to become available on the restarted computer before it continues.
    # Restart-Computer -Wait -For PowerShell -Timeout 300 -Delay 2

}

else {
    Write-Output "Chocolatey Version $testchoco is already installed"
    choco feature enable -n=allowGlobalConfirmation

    choco upgrade chocolatey
}

function Install-App-With-Choco{
	Param(
	[parameter(Mandatory=$true)][String]$AppName 
	)
	choco install "$AppName" -A --ignore-checksums;

}

# following will fail if not restarted
Install-App-With-Choco "googlechrome"
Install-App-With-Choco "firefox"
Install-App-With-Choco "google-backup-and-sync"
Install-App-With-Choco "google-drive-file-stream"
Install-App-With-Choco "vlc"
Install-App-With-Choco "7zip"
Install-App-With-Choco "malwarebytes"
Install-App-With-Choco "sublimetext3.app"
Install-App-With-Choco "treesizefree"
Install-App-With-Choco "wiztree"
Install-App-With-Choco "everything"

Write-Output "End Heyo1"
Stop-Transcript
