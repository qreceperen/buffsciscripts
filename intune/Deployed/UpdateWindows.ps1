
$log_file_name_start = "UpdateWindows.ps1";
$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}
$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"


install-module RunAsUser -Force

Install-Module PSWindowsUpdate -Force
Import-Module -Name PSWindowsUpdate

Get-WindowsUpdate
Get-WindowsUpdate -AcceptAll -Install #-AutoReboot

$winVer = [System.Environment]::OSVersion.Version.Major
$dir = 'C:\temp'
mkdir $dir -ErrorAction SilentlyContinue

if ($winVer -eq 10) {  
    $webClient = New-Object System.Net.WebClient
    $url = 'https://go.microsoft.com/fwlink/?LinkID=799445'
    $file = "$($dir)\Win10Upgrade.exe"
    $webClient.DownloadFile($url, $file)
    Start-Process -FilePath $file -ArgumentList '/quietinstall /skipeula /auto upgrade /copylogs $dir'
}
else {
    Write-Output "This is Not Windows10 OS "
}
    
sleep 10
# Remove-Item "C:\temp" -Recurse -Force -Confirm:$false


# $scriptblock = {
#     Import-Module AnyBox
#     Show-AnyBox -Title 'BuffSci IT: Please Restart ASAP!' -Message 'Latest Windows update problem is fixed now. Please Restart your computer as soon as possible today.' -FontSize 16 -ContentAlignment 'Center' -Buttons 'OK' -Topmost `
#         -Icon 'Warning' -BackgroundColor 'White' -FontColor 'Blue' -FontFamily 'Comic Sans MS'
    
#     Import-Module BurntToast
#     New-BurntToastNotification -Text 'BuffSci IT: Please Restart ASAP!', 'Latest Windows update problem is fixed now. Please Restart your computer as soon as possible today.' -Sound 'Alarm2' -SnoozeAndDismiss        
#     }

# try {
#     Invoke-AsCurrentUser -scriptblock $scriptblock
# } catch{
#     write-error "Something went wrong with Invoke-AsCurrentUser"
# }
# start-sleep 2 #Sleeping 2 seconds to allow script to write to disk.


Write-Output "End Heyo1"
Stop-Transcript
