$users=(ls C:\Users)

Foreach ($user in $users){
    $user_name = $user.name

        Write-Output "-> checking $user";
        if ( $user_name -ne "Public" -and $user_name -notlike "defaultuser*" -and $user_name -ne "gaia" ) {
            Write-Output $user_name
            Write-Output "--> $user"
        

       mkdir "C:\Users\$user_name\AppData\Local\Google\Drive\user_default" -force;


       $String1 = @" 
[Computers]
desktop_enabled: True
documents_enabled: True
pictures_enabled: False
folders: C:\Users\$user_name\Downloads
high_quality_enabled: False
always_show_in_photos: False
usb_sync_enabled: False
ignore_extensions: ext1, ext2, ext3
# Delete mode can be: ALWAYS_SYNC_DELETES, ASK, NEVER_SYNC_DELETES
delete_mode: NEVER_SYNC_DELETES
[MyDrive]
folder: /path/to/google_drive
my_drive_enabled: False
[Settings]
autolaunch: True
show_overlays: False
[Network]
download_bandwidth: 100
upload_bandwidth: 200
use_direct_connection: False
"@

$String1 | Out-File "C:\Users\$user_name\AppData\Local\Google\Drive\user_default\user_setup.config"


}
}

# chown -R "$user_name:staff" "/Users/$user_name/Library/Application Support/Google"

# TODO: will need to modify file permission to work
$sharepath = "C:\Users\IntuneDEM\AppData\Local\Google\Drive\user_default\user_setup.config"
# get-acl $sharepath | Format-List

$Acl = Get-ACL $SharePath

$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("AzureAD\BuffSciITDepartment","FullControl","Allow")
$Acl.AddAccessRule($AccessRule)

$Group = New-Object System.Security.Principal.NTAccount("AzureAD", "BuffSciITDepartment")
$ACL.SetOwner($Group)
$ACL.SetGroup($Group)
Set-Acl $SharePath -AclObject $ACL


Get-ACL $SharePath | Format-List



$currUser = $env:UserName
$currUserHome = $Home
Write-Output $currUser

$String1 = @" 
[Computers]
desktop_enabled: True
documents_enabled: True
pictures_enabled: False
folders: $currUserHome\Downloads
high_quality_enabled: False
always_show_in_photos: False
usb_sync_enabled: False
ignore_extensions: ext1, ext2, ext3
# Delete mode can be: ALWAYS_SYNC_DELETES, ASK, NEVER_SYNC_DELETES
delete_mode: NEVER_SYNC_DELETES
[MyDrive]
folder: /path/to/google_drive
my_drive_enabled: False
[Settings]
autolaunch: True
show_overlays: False
[Network]
download_bandwidth: 100
upload_bandwidth: 200
use_direct_connection: False
"@

$String1 | Out-File "$currUserHome\AppData\Local\Google\Drive\user_default\user_setup.config"

$acl = Get-Acl "$currUserHome\AppData\Local\Google\Drive\user_default\user_setup.config"
$accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("everyone","FullControl","Allow")
$acl.SetAccessRule($accessRule)
$acl.SetAccessRuleProtection($true, $false)

$acl | Set-Acl "$currUserHome\AppData\Local\Google\Drive\user_default\user_setup.config"
