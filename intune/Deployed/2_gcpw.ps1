<# 

This script downloads Google Credential Provider for Windows from
https://tools.google.com/dlpage/gcpw/, then installs and configures it.
Windows administrator access is required to use the script. #>

# .\PSTools\PsExec.exe -i -s .\gcpw.ps1

# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine
# Get-ExecutionPolicy -List



<# Set the following key to the domains you want to allow users to sign in from.

For example:
$domainsAllowedToLogin = "acme1.com,acme2.com"

#>

$log_file_name_start = "2_gcpw.ps1_1";
$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}
$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"

Clear-Host
$ErrorActionPreference = 'Continue'
$VerbosePreference = 'Continue'
$USERNAME = "gaia"
$ObjLocalUser = $null

Try {
    Write-Verbose "Searching for $($USERNAME) in LocalUser DataBase"
    $ObjLocalUser = Get-LocalUser $USERNAME
    Write-Verbose "User $($USERNAME) was found"
}
Catch [Microsoft.PowerShell.Commands.UserNotFoundException] {
    "User $($USERNAME) was not found" | Write-Warning
}
Catch {
    "An unspecifed error occured" | Write-Error
}

#Create the user if it was not found (Example)
If ($ObjLocalUser) {
    Write-Verbose "User $($USERNAME) exists, exiting"
    Write-Output "Ending early1 Heyo1"
    Stop-Transcript
    Exit # Stop Powershell! 
}


Add-Type -AssemblyName System.speech
$speak = New-Object System.Speech.Synthesis.SpeechSynthesizer

$domainsAllowedToLogin = "bascs.org,buffsci.org"
$name = 'domains_allowed_to_login'
$domains = Get-ItemPropertyValue HKLM:\Software\Google\GCPW -Name $name -ErrorAction SilentlyContinue

if ($domains -eq $domainsAllowedToLogin) {
    Write-Output "GCPW is already installed and configured, Exiting with 5!"
    Write-Output "Ending early2 Heyo1"
    Stop-Transcript
    exit 5
} else {
    Write-Output "Will install GCPW!"
    $speak.Speak('Will install GCPW!')
}

Add-Type -AssemblyName System.Drawing
Add-Type -AssemblyName PresentationFramework

<# Check if one or more domains are set #>
if ($domainsAllowedToLogin.Equals('')) {
    # $msgResult = [System.Windows.MessageBox]::Show('The list of domains cannot be empty! Please edit this script.', 'GCPW', 'OK', 'Error')
    Write-Output "The list of domains cannot be empty! Please edit this script."
    exit 5
}

function IsAdmin() {
    $admin = [bool](([System.Security.Principal.WindowsIdentity]::GetCurrent()).groups -match 'S-1-5-32-544')
    return $admin
}

<# Check if the current user is an admin and exit if they aren't. #>
if (-not (IsAdmin)) {
    # $result = [System.Windows.MessageBox]::Show('Please run as administrator!', 'GCPW', 'OK', 'Error')
    Write-Output "Please run as administrator!"
    exit 5
}

<# Choose the GCPW file to download. 32-bit and 64-bit versions have different names #>
$gcpwFileName = 'gcpwstandaloneenterprise.msi'
if ([Environment]::Is64BitOperatingSystem) {
    $gcpwFileName = 'gcpwstandaloneenterprise64.msi'
}

# Check if msi file is there

<# Download the GCPW installer. #>
$gcpwUrlPrefix = 'https://dl.google.com/credentialprovider/'
$gcpwUri = $gcpwUrlPrefix + $gcpwFileName
Write-Host 'Downloading GCPW from' $gcpwUri
Invoke-WebRequest -Uri $gcpwUri -OutFile $gcpwFileName

<# Run the GCPW installer and wait for the installation to finish #>
$arguments = "/i `"$gcpwFileName`""
$installProcess = (Start-Process msiexec.exe -ArgumentList $arguments -PassThru -Wait)

<# Check if installation was successful #>
if ($installProcess.ExitCode -ne 0) {
    # $result = [System.Windows.MessageBox]::Show('Installation failed!', 'GCPW', 'OK', 'Error')
    Write-Output "Ending early2 Heyo1, exit code is $installProcess.ExitCode"
    Stop-Transcript

    exit $installProcess.ExitCode
    Write-Output "Installation failed"
}
else {
    # $result = [System.Windows.MessageBox]::Show('Installation completed successfully!', 'GCPW', 'OK', 'Info')
    Write-Output "Installation completed successfully!"
}

<# Set the required registry key with the allowed domains #>
$registryPath = 'HKEY_LOCAL_MACHINE\Software\Google\GCPW'

[microsoft.win32.registry]::SetValue($registryPath, $name, $domainsAllowedToLogin)

$domains = Get-ItemPropertyValue HKLM:\Software\Google\GCPW -Name $name

if ($domains -eq $domainsAllowedToLogin) {
    # $msgResult = [System.Windows.MessageBox]::Show('Configuration completed successfully!', 'GCPW', 'OK', 'Info')
    Write-Output "Configuration completed successfully, Restarting!"
    $speak.Speak('Configuration completed successfully, Restarting!')

    # Restart-Computer
}
else {
    # $msgResult = [System.Windows.MessageBox]::Show('Could not write to registry. Configuration was not completed.', 'GCPW', 'OK', 'Error')
    Write-Output "Could not write to registry. Configuration was not completed."
}

Write-Output "End Heyo1"
Stop-Transcript

