
$log_file_name_start = "5_printers.ps1_1";
$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}

$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"



# Set-ExecutionPolicy remotesigned
$log_file_name = "printers";

$mostRecentFile = ls "C:\temp" -Filter "$log_file_name.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}

$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$log_file = "C:\temp\$log_file_name.$theDate.$runVer.log"



$OFS = "`r`n";
Write-Host '----->Running printers.ps1 on '+$(Get-Date)  *> $log_file;
Add-Content -Path $log_file -Value "$OFS $OFS";

Write-Host "HOME is "+$HOME *>> $log_file;
Add-Content -Path $log_file -Value "$OFS $OFS";

Write-Host "Computername and Username: $env:computername-$env:UserName" *>> $log_file;
Add-Content -Path $log_file -Value "$OFS $OFS";

# KX Print Driver (8.0.1329)
# $url = "https://usa.kyoceradocumentsolutions.com/content/dam/kdc/kdag/downloads/technical/executables/drivers/kyoceradocumentsolutions/us/en/KX801329.zip"

# Kx_8.1.1109_UPD_Signed_EU
$url="https://www.kyoceradocumentsolutions.eu/content/download-center/eu/drivers/all/KX_Universal_Printer_Driver_zip.download.zip"

# KX Print Driver (8.1.1109)
# https://ca.kyoceradocumentsolutions.com/content/dam/kdc/kdag/downloads/technical/executables/drivers/kyoceradocumentsolutions/ca/en/KX811109.exe
# $url="https://www.kyoceradocumentsolutions.us/content/download-center-americas/us/drivers/drivers/KX811109_exe.download.exe"
    
#Download file
# $file = "c:\temp\KX_Print_Driver.zip"
$file = "c:\temp\"+(Split-Path $url -leaf)

if (Test-Path "$file") {
    Write-Host "$file is already downloaded"  *> $log_file;
    Add-Content -Path $log_file -Value "$OFS $OFS";

} else {
    # $clnt = new-object System.Net.WebClient

    # $clnt.DownloadFile($url, $file)
    powershell -command "Invoke-WebRequest -Uri '$url' -OutFile '$file'" 3>&1 2>&1 >> $log_file;
}

Start-Sleep 5



Write-Output "---> Before Unzip file" *>> $log_file;
# $extracted_file_name = "KX801329"
# $extracted_file_name = "KX_Universal_Printer_Driver_zip.download"
$extracted_file_name = "Kx_8.1.1109_UPD_Signed_EU"

if (Test-Path "C:\temp\$extracted_file_name") {
    # Remove-Item "C:\temp\$extracted_file_name" -Recurse  

    Write-Host "$extracted_file_name is already unzipped"   *> $log_file;
    Add-Content -Path $log_file -Value "$OFS $OFS";
}
else {
    $shell_app = new-object -com shell.application 
    # $filename = "KX_Print_Driver.zip" 
    # $zip_file = $shell_app.namespace("C:\temp" + "\$filename")

    $zip_file = $shell_app.namespace("$file")

    $destination = $shell_app.namespace("C:\temp") 
    # 4 silent & overwrite
    $destination.Copyhere($zip_file.items(), 0x14)
}

Write-Output "---> After Unzip file" *>> $log_file;


#Install Printer
Write-Output "---> Before  pnputil" *>> $log_file;
Invoke-Command { pnputil.exe -a "C:\Temp\$extracted_file_name\en\64bit\OEMSETUP.INF" };
Write-Output "---> After pnputil" *>> $log_file;


<#
Invoke-Command  -ScriptBlock { Start-Process $file -ArgumentList "/S /v/qn" -Wait}
Invoke-Command  -ScriptBlock { Start-Process $file -ArgumentList "/S" -Wait}
#>

$b_w_driver = "Kyocera TASKalfa 6003i KX"
$c_driver = "Kyocera TASKalfa 5053ci KX" #5551ci?

Write-Output "---> Before Add-PrinterDriver" *>> $log_file;
powershell -command "Add-PrinterDriver -Name '$b_w_driver'" 3>&1 2>&1 >> $log_file;
powershell -command "Add-PrinterDriver -Name '$c_driver'" 3>&1 2>&1 >> $log_file;
Write-Output "---> After Add-PrinterDriver" *>> $log_file;

# Get-PrinterDriver

Function addPrinter {
    param (
        $printer_name,
        $queue_name,
        $driver
    )
    Write-Output "Adding Printer_Name "+ $printer_name  *>> $log_file;
    Write-Output "Adding Printer_Driver "+ $driver  *>> $log_file;

    powershell -command "Add-PrinterPort -Name '$printer_name' -LprHostAddress '172.16.16.4' -LprQueueName '$queue_name' -LprByteCounting" 3>&1 2>&1 >> $log_file;
    powershell -command "Add-Printer -Name '$printer_name' -DriverName '$driver' -PortName '$printer_name' -SeparatorPageFile 'C:\Windows\System32\sysprtj.sep'" 3>&1 2>&1 >> $log_file;
}

# Remove-PrinterPort -Name "ES-Printers B/W"
# Remove-PrinterPort -Name "MS-Printers B/W"
# Remove-PrinterPort -Name "HS-Printers B/W"

# When updating lpr queue names, might need to remove a port 
# and add a new port and associate new port with printer

Rename-Printer -name "HS-Printers B/W" -newName "Franklin-Printers B/W" -ErrorAction SilentlyContinue
Rename-Printer -name "HS-Color" -newName "Franklin-Color" -ErrorAction SilentlyContinue

Rename-Printer -name "MS-Printers B/W" -newName "Poplar-Printers B/W" -ErrorAction SilentlyContinue
Rename-Printer -name "MS-Color" -newName "Poplar-Color" -ErrorAction SilentlyContinue

Rename-Printer -name "ES-Printers B/W" -newName "Clare-Printers B/W" -ErrorAction SilentlyContinue
Rename-Printer -name "ES-Color" -newName "Clare-Color" -ErrorAction SilentlyContinue


addPrinter "Franklin-Color" "HS-Color" $c_driver
Set-Printer -name "Franklin-Color" -PortName "Franklin-Color"
addPrinter "Franklin-Printers B/W" "HS-Printers" $b_w_driver
Set-Printer -name "Franklin-Printers B/W" -PortName "Franklin-Printers B/W"

addPrinter "Poplar-Color" "MS-Color" $c_driver
Set-Printer -name "Poplar-Color" -PortName "Poplar-Color"
addPrinter "Poplar-Printers B/W" "MS-Printers" $b_w_driver
Set-Printer -name "Poplar-Printers B/W" -PortName "Poplar-Printers B/W"

addPrinter "Clare-Color" "ES-Color" $c_driver
Set-Printer -name "Clare-Color" -PortName "Clare-Color"
addPrinter "Clare-Printers B/W" "ES-Printers" $b_w_driver
Set-Printer -name "Clare-Printers B/W" -PortName "Clare-Printers B/W"

addPrinter "Doat-Printers B/W" "Doat-Printers" $b_w_driver
Set-Printer -name "Doat-Printers B/W" -PortName "Doat-Printers B/W"
# Set-Printer -name "Doat-Printers B/W" -SeparatorPageFile "C:\Windows\System32\sysprtj.sep"




############################################################################### 
 
###########Define Variables######## 
 
$fromaddress = "helpdesk@bascs.org" 
$toaddress = "sen@bascs.org" 
# $bccaddress = "Vikas.sukhija@labtest.com" 
# $CCaddress = "Mahesh.Sharma@labtest.com" 
$Subject = "$env:computername-$env:UserName-printers_01.ps1" 
$body = get-content $log_file
$attachment = $log_file
$smtpserver = "smtp.gmail.com" 
$smtpport = "587" 

# Sender Credentials
$Username = "helpdesk@bascs.org"
$Password = "helpdesk?2020?"

#################################### 
 
$message = new-object System.Net.Mail.MailMessage 
$message.From = $fromaddress 
$message.To.Add($toaddress) 
# $message.CC.Add($CCaddress) 
# $message.Bcc.Add($bccaddress) 
$message.IsBodyHtml = $True 
$message.Subject = $Subject 
$attach = new-object Net.Mail.Attachment($attachment) 
$message.Attachments.Add($attach) 
$message.body = $body 
$smtp = new-object Net.Mail.SmtpClient($smtpserver, $smtpport) 

$smtp.EnableSsl = $true
$smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password)

# $smtp.Send($message) 
 
#################################################################################




# Get-PrinterProperty -PrinterName "HS-Printer1"

# $printers = get-printer * 
# foreach ($printer in $printers)
# { 
#     get-printerproperty -printerName $printer.name 
# }



# foreach ($PrinterName in $PrinterNames ){
#     $P = Get-Printer $PrinterNames.PrinterName
#     Remove-PrinterPort -Name $P.PortName
#     Add-PrinterPort -Name $PrinterName.NewPortName -PrinterHostAddress $PrinterName.NewPortAddress
#     Set-Printer -name $p -PortName $PrinterName.NewPortName
#   }
  
Write-Output "End Heyo1"
Stop-Transcript