$log_file_name_start = "4_Uninstall-App1.ps1_1";
$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}
$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"


function Uninstall-App([String] $AppName = "Editor") {
    Write-Output "1. Trying to uninstall app $AppName"
    $MyApp = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -eq "$AppName" }

    if ($MyApp) {
        Write-Output "2. Uninstalling app $AppName"
        $MyApp.Uninstall()
    }
    else {
        Write-Output "3. No app with name $AppName"
    }

}

Uninstall-App "Admin By Request Workstation"


Write-Output "End Heyo1"
Stop-Transcript