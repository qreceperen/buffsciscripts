$log_file_name_start="install_exe";

$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;

if($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[1] -as [int];
    $runVer = $runVer + 1;
} else {
    $runVer = 1;
}

$transcriptPath="C:\temp\$log_file_name_start.$runVer.log"

Start-Transcript -Path $transcriptPath -Force # -NoClobber


# $download_uri="https://drive.google.com/uc?export=download&id=1BQYuxXQVN_ftV1gDNjpErzIK22KET8ON";
# $fileName="Document_Camera_Webcam_Viewer.exe";

# $download_uri="https://www.clearlyip.com/downloads/clearlyanywhere/clearlyanywhere-1_0_3_75-windows.exe";
$download_uri="https://clearlyanywhere.app/app/windows";
$fileName="clearlyanywhere.exe";


mkdir "C:\temp" -force;
$log_file = "C:\temp\dir.log"

$OFS = "`r`n";
Write-Host '----->Downloading '$fileName' from' $download_uri *> "$log_file";
Add-Content -Path "$log_file" -Value "$OFS $OFS";

# -RedirectStandardOutput $stdOutLog -RedirectStandardError $stdErrLog -wait
# Get-Content $stdErrLog, $stdOutLog | Out-File $myLog -Append

# $ProgressPreference = 'SilentlyContinue'
# powershell -command "Invoke-WebRequest -Uri '$download_uri' -OutFile 'C:\temp\$fileName'" 3>&1 2>&1 `
# | Out-File "$log_file" -Append;

$wc = New-Object net.webclient
$wc.Downloadfile($download_uri, 'C:\temp\'+$fileName)

Start-Sleep -Seconds 20

Invoke-Command  -ScriptBlock { Start-Process 'C:\temp\clearlyanywhere.exe' -ArgumentList "/S /v/qn" -Wait}

Start-Sleep -Seconds 20

$User_Home="C:\Users\$env:UserName"
Write-Host $User_Home
# $WshShell = New-Object -comObject WScript.Shell
# $Shortcut = $WshShell.CreateShortcut("$User_Home\Desktop\ClearlyAnywhere.lnk")
# $Shortcut.TargetPath = "C:\Program Files\WIndowApps\MyApp\MyApp.exe"
# $Shortcut.Save()


# powershell -command "Move-Item -Path 'C:\temp\$fileName' -Destination $HOME\Desktop\$fileName -Force" 3>&1 2>&1 >> "$log_file";


# C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup


############################################################################### 
 
$fromaddress = "helpdesk@bascs.org" 
$toaddress = "sen@bascs.org" 
# $bccaddress = "Vikas.sukhija@labtest.com" 
# $CCaddress = "Mahesh.Sharma@labtest.com" 
$Subject = "$env:computername-$env:UserName-Report-install_exe.ps1" 
$body = get-content "$log_file"
$attachment = "$log_file"
$smtpserver = "smtp.gmail.com" 
$smtpport = "587" 

# Sender Credentials
$Username = "helpdesk@bascs.org"
# $Password = "helpdesk?2020?"
$Password="dvemmzbkjnpmxczw"

#################################### 
 
$message = new-object System.Net.Mail.MailMessage 
$message.From = $fromaddress 
$message.To.Add($toaddress) 
# $message.CC.Add($CCaddress) 
# $message.Bcc.Add($bccaddress) 
$message.IsBodyHtml = $True 
$message.Subject = $Subject 
$attach = new-object Net.Mail.Attachment($attachment) 
$message.Attachments.Add($attach) 
$message.body = $body 
$smtp = new-object Net.Mail.SmtpClient($smtpserver, $smtpport) 

$smtp.EnableSsl = $true
$smtp.Credentials = New-Object System.Net.NetworkCredential($Username,$Password)

# $smtp.Send($message) 
 
#################################################################################

Stop-Transcript

