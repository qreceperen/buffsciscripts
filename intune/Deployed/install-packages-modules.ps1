$log_file_name_start = "install-packages-modules.ps1";

$mostRecentFile = ls "C:\temp" -Filter "$log_file_name_start.*.log" | sort  -Descending | Select-Object -Index 0;
if ($mostRecentFile) {
    $runVer = $mostRecentFile.Name.Split(".")[-2] -as [int];
    $runVer = $runVer + 1;
}
else {
    $runVer = 1;
}
$theDate = Get-Date -format "yyyy-MM-dd_a\t_HH-mm-ss"
$transcriptPath = "C:\temp\$log_file_name_start.$theDate.$runVer.log"
Start-Transcript -Path $transcriptPath -Force # -NoClobber
Write-Output "Start Heyo1"


Write-Output "Trying to install NuGet"
# if ((Get-PackageProvider -Name NuGet).version -lt '2.8.5.208' ) {
#     Write-Output "Trying to install NuGet -MinimumVersion 2.8.5.208"
# try {
#         Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.208 -Confirm:$False -Force 
#     } catch [Exception] {
#         $_.message 
#         # exit
#     }
# Write-Output "After Trying to install NuGet"
# } else {
#     Write-Host "NuGet is already installed = " (Get-PackageProvider -Name NuGet).version
# }

# Make sure TLS 1.2 is enabled (All Microsoft platforms require it.)
[Net.ServicePointManager]::SecurityProtocol = [Enum]::ToObject([Net.SecurityProtocolType], 3072)

# Make sure PSGet and NuGet are up to date.
Install-PackageProvider NuGet -Force
Install-Module PSWindowsUpdate -Force
Install-Module -Name PowerShellGet -Force -AllowClobber
Update-Module -Name PowerShellGet

# And finally, update any installed modules that reference the old PSGallery URL.
Get-InstalledModule | Where-Object { $_.RepositorySourceLocation -eq 'https://www.powershellgallery.com/api/v2/' } | ForEach-Object { Install-Package -Name $_.Name -Source PSGallery -Force -AcceptLicense }



if (!(Get-Module -ListAvailable -Name AnyBox)) {
    Install-Module -Name 'AnyBox' -Force
} 

if (!(Get-Module -ListAvailable -Name BurntToast)) {
    Install-Module -Name 'BurntToast' -Force
} 

Write-Output "End Heyo1"
Stop-Transcript
