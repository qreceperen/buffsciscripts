[string]$GoogleFileId = "1sag-0QbxDrv1HBjpUWkghzTP58X1pEWH"
[string]$FileDestination = "C:\dch_igcc_win64_25.20.100.6618.zip"
# set protocol to tls version 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
# Download the Virus Warning into _tmp.txt
Invoke-WebRequest -Uri "https://drive.google.com/uc?export=download&id=$GoogleFileId" -OutFile "_tmp.txt" -SessionVariable googleDriveSession
# Get confirmation code from _tmp.txt
$searchString = Select-String -Path "_tmp.txt" -Pattern "confirm="
$searchString -match "confirm=(?<content>.*)&amp;id="
$confirmCode = $matches['content']
# Delete _tmp.txt
Remove-Item "_tmp.txt"
# Download the real file
$progresspreference='silentlycontinue'
Invoke-WebRequest -Uri "https://drive.google.com/uc?export=download&confirm=${confirmCode}&id=$GoogleFileId" -OutFile $FileDestination -WebSession $googleDriveSession
$progresspreference='continue'
start-sleep -seconds 300
expand-archive -path 'C:\dch_igcc_win64_25.20.100.6618.zip' -destinationpath 'C:\DriverFix'
pnputil /delete-driver ((Get-WmiObject Win32_pnpsigneddriver | where {$_.devicename -like '*UHD*'}).InfName) /uninstall /force
cd C:\DriverFix
C:\DriverFix\igxpin.exe -overwrite -s
$AppsToRemove = @(
'Drivers As A Service'
)
$RegKeys = @(
'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\'
'HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\'
)
$Apps = $RegKeys |
Get-ChildItem |
Get-ItemProperty |
Where-Object { $AppsToRemove -contains $_.DisplayName -and $_.UninstallString }
foreach( $App in $Apps ){
$UninstallString = if( $App.Uninstallstring -match '^msiexec' ){
"$( $App.UninstallString -replace '/I', '/X' ) /qn /norestart"
}
else{
$App.UninstallString
}
Write-Verbose $UninstallString
Start-Process -FilePath cmd -ArgumentList '/c', $UninstallString -NoNewWindow -Wait
}
